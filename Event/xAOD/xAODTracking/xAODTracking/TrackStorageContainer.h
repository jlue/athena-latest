/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODTRACKING_TRACKSTORAGECONTAINER_H
#define XAODTRACKING_TRACKSTORAGECONTAINER_H

#include "xAODTracking/versions/TrackStorageContainer_v1.h"

namespace xAOD {
  typedef TrackStorageContainer_v1 TrackStorageContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackStorageContainer , 1290720542 , 1 )
#endif